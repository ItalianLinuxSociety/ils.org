#!/bin/sh

# Allow custom Nikola commands without "nikola" prefix.
# So this may be empty, or "build" or "serve" etc.
GENERIC_COMMAND="$@"

IS_SERVING=0

# As default, run Nikola "serve".
# The external port is specified in the .env file.
# From inside the Container, listen on all interfaces.
# Output should be ignored to avoid confusion. Since "8000" is just the internal port.
if [ -z "$GENERIC_COMMAND" ]; then
	GENERIC_COMMAND="serve --port=8000"
	IS_SERVING=1
fi

# Expand the command to be a nikola command.
NIKOLA_COMMAND="nikola $GENERIC_COMMAND"

# Troubleshoot what we are doing.
printf "%s\n" "$NIKOLA_COMMAND"

if [ "$IS_SERVING" = 1 ]; then

	# Sometime, "nikola build" has a very aggresive cache,
	# so let's kill this damn cache, otherwise it seems that nothing works.
	python -m nikola clean

	# Sometime, "nikola serve" does not automatically build and could die with:
	#  ERROR: serve: Missing 'output' folder?
	# So we manually build again before serving.
	python -m nikola build

	# Import environment variables to show something nice to the user.
	if [ -f ./.env ]; then
		. ./.env
	fi

	echo " __________"
	echo "< Running! >"
	echo " ----------"
	echo "        \   ^__^"
	echo "         \  (oo)\_______"
	echo "            (__)\       )\/\ "
	echo "                ||----w |"
	echo "                ||     ||"

	if [ -n "$NIKOLA_SERVE_PORT" ]; then

		# Generate a nice "Visit this address <link>" whatever the Docker port.
		# For interesting reasons the "port" in Docker may contain an hostname or not.
		# Is the string needing a dummy hostname?
		NIKOLA_SERVE_HOST=
		if [ "${NIKOLA_SERVE_PORT#*":"}" = "$NIKOLA_SERVE_PORT" ]; then
			NIKOLA_SERVE_HOST="localhost:"
		fi

		echo "Visit this address:"
		echo "  http://$NIKOLA_SERVE_HOST""$NIKOLA_SERVE_PORT""/"
	fi

	echo
	echo "  Edit the file .env to change that port (NIKOLA_SERVE_PORT)."
	echo
	echo "(Press CTRL+C to quit)"
	echo
	echo "IMPORTANT: Ignore the port mentioned under this message :)"
fi

# TODO: Improve nikola to suppress its INFO output so it does not mention its
# internal port during a "nikola serve".
python -m $NIKOLA_COMMAND
