---
layout: page
title: Rimborsi Spese - Sostegno Economico
description: Italian Linux Society sostiene il software libero investendo le risorse dell'associazione in iniziative promosse da volontari e volontarie.
---

Italian Linux Society sostiene il software libero investendo le risorse dell'associazione in iniziative promosse da volontari e volontarie.

Italian Linux Society è un'associazione di volontariato e non può quindi pagare i volontari. Possono essere sostenuti i costi di acquisto e produzione di materiale promozionale, spese di pulizia delle sale, ecc.

[TOC]

## Requisiti Minimi

Le richieste di sostegno economico sono approvabili se rispettano i seguenti requisiti minimi:

1. La richiesta implementa un'iniziativa che **contribuisce** agli **scopi** di Italian Linux Society (elencati nello [statuto](/statuto/)). Esempi:
	* ✅ Rimborsi per spese di pulizia di una sala in cui si è tenuto un evento sul software libero (inclusi ovviamente i Linux Day), ecc.
	* ✅ Rimborsi per acquisto di adesivi, spille, ecc. per la promozione del software libero, ecc.
	* ❌ Organizzare un corso su Photoshop (o altro software proprietario) usando Linux. Usare GNU/Linux come mezzo secondario per diffondere primariamente software proprietario non è normalmente ammessibile.
2. C'è una persona **proponente** che si prende cura della proposta secondo questi requisiti, ed è a disposizione per chiarimenti o discutere di eventuali compromessi.
3. La richiesta evidenzia il **totale** massimo di spesa prevista (IVA inclusa) e allega almeno un preventivo. Esempi:
	* ✅ _fra una settimana vorrei acquistare 300€ in adesivi e spille, ecco lo screenshot del carrello di un fornitore. Le quantità indicate ci sembrano adeguate per i nostri circa XXX partecipanti attesi. Se avanzano li terremo per altri eventi futuri._
4. L'iniziativa ha una **data di inizio** e di fine. Esempi:
	* ✅ _in data XXX si terrà il Linux Day CITTÀ, più un evento accompagnatorio, quest'ultimo in data YYY da confermare ecc._
5. Le persone legate alla richiesta dichiarano i loro **potenziali conflitti di interessi**.
	* ⚡ Se il tuo fornitore di fiducia è tuo parente, devi comunicarlo chiaramente e fornire almeno altri due preventivi da altri fornitori.
6. La persona proponente si impegna a pubblicare una **breve relazione** ed **entro tre mesi** dal termine dell'iniziativa. Esempi:
	* ✅ Descrivi come si è svolta l'attività, i risultati attesi, i risultati ottenuti, i problemi emersi e dei miglioramenti da prevedere per simili attività in futuro.
	* ✅ Puoi creare una pagina sul tuo sito web o sul forum di ILS sezione Progetti. Ancora meglio se includi qualche foto in licenza libera CC BY-SA e l'inicazione di chi ha realizzato l'immagine.

## Linee Guida Aggiuntive

L'approvazione delle richieste è soggetta anche alle seguenti linee guida aggiuntive - soprattutto quando ne riceviamo molte o quando le risorse iniziano ad esaurirsi.

Le seguenti linee guida sono elencate dalla più importante, alla meno importante:

* Tutte le persone coinvolte nell'iniziativa fanno <b>volontariato</b>.
	* ⚡ Esempio: Segnala in modo evidente tutti gli eventuali relatori retribuiti, poiché in generale questo non è ammesso.
* La richiesta di sostegno economico è inviata con un <b>gentile anticipo</b>. Esempi: 
	* ✅ Esempio: Le richieste economiche presentate con 3 settimane di anticipo sono solitamente avvantaggiate.
	* ⚡ Esempio: Le richieste straordinarie presentate 2 giorni prima dell'inizio o addirittura 20 minuti dopo aver effettuato la spesa devono essere maggiormente giustificate.
* La richiesta è accompagnata da più preventivi.
	* ✅ A chi dona fa piacere sapere che ti stai prendendo cura del tuo budget e che hai valutato almeno un'altra opzione magari potenzialmente migliore.
	* ✅ Esempio: Ogni ulteriore preventivo che vuoi allegare è ben visto (senza esagerare: tre bastano).
* Importo inferiore a <b>1000 EURO</b> / anno.
	* ⚡ Esempio: in un singolo anno non riusciamo generalmente a fornire più di questa cifra ad una stessa città o ad uno stesso user group.
* Importo individuale inferiore a <b>220 EURO</b> / persona / anno.
	* ✅ Esempio: siamo lieti di aiutare certe richieste strettamente individuali di trasporto / vitto / alloggio a relatori e soprattutto se si è _keynote_ nel programma dell'evento ma
	* ⚡ Superare il limite indicato rischia di limitare la capacità di Italian Linux Society nel sostenere altre iniziative più generali, altri user group, altri eventi. Grazie per la comprensione.
* L'iniziativa coinvolge altri partner e co-finanziatori.
	* ✅ Aver trovato altri partner (associazioni, università, enti pubblici, media partner ecc.) o altri co-finanziatori (donatori, sponsor, forniture gratuite, fondazioni erogatrici, fondi pubblici) è un'ottima cosa.
	* ✅ Chiarisci chiaramente su quali risorse stai richiedendo sostegno economico, e su quali invece hai già ottenuto una copertura (esempio: la sala è già offerta da... il pranzo è già offerto da ...)
* La proposta è già condivisa e invita alla discussione e partecipazione.
	* ✅ Ottimo se la tua iniziativa è già pubblicata online per richieste di pareri, per esempio sul [forum Linux.it](https://forum.linux.it/) sezione "Progetti".
* L'iniziativa è una richiesta nata internamente a Italian Linux Society.
	* ✅ Ottimo se chi propone è una persona associata ad Italian Linux Society.
	* ✅ Ottimo se chi propone fa parte di una sezione locale di Italian Linux Society.
	* ✅ Ottimo se si è attivi in un user group riconosciuto dalla [LugMap](https://lugmap.linux.it/).
* L'iniziativa non include <b>rimborso di viaggi</b>. I rimborsi di viaggio sono raramente finanziati e secondo i seguenti criteri:
	* ✅ La persona richiedente è nell'organizzazione ufficiale dell'evento o almeno ha una sessione ufficiale, magari _keynote_
	* ✅ treni: preferenza a biglietti in classe economy
	* ⚡ automobile: da evitare - soprattutto se si viaggia da soli. È difficile da quantificare ed è poco sostenibile.
* L'iniziativa considera la sostenibilità ambientale.

Come già accennato queste sono soltanto linee guida aggiuntive e si può in generale uscire da questi schemi ove necessario, ma non senza un'opportuna giustificazione aggiuntiva.

## Modalità di Rimborso

Puoi decidere tu, direttamente nella tua richiesta, come preferisci che Italian Linux Society proceda a sostenerti.

In breve, Italian Linux Society può pagare direttamente le spese (opzione 1 - consigliata) oppure puoi anticipare tu tutti i pagamenti e ti rimborsiamo successivamente (opzione 2).

<div class="row">
	<div class="col s12 m6 card">
		<h3>Opzione 1<br />Acquisto diretto da Italian Linux Society</h3>
		<p><em>Italian Linux Society può eventualmente pagare tutto al posto tuo cosicché non devi anticipare nulla.</em></p>
		<p>
		<b>Vantaggi:</b><br />
			<ul>
				<li>La persona richiedente non anticipa nulla</li>
				<li>È ideale anche per la commercialistica di ILS, perché paga ILS e la fattura è intestata ad ILS.</li>
			</ul>
		<b>Svantaggi:</b><br />
			<ul>
				<li>devi fornire a ILS tutti i dettagli su ciò che serve acquistare (caratteristiche precise, quantità, ecc.) e tutti i dettagli di spedizione</li>
				<li>potrebbero esserci ulteriori rallentamenti di acquisto e quindi di spedizione se la segreteria di ILS è sovraccarica (ma tu non richiedi le cose all'ultimo con urgenza, giusto?)</li>
			</ul>
		</p>
	</div>
	<div class="col s12 m6 card">
		<h3>Opzione 2:<br />Rimborso</h3>
		<p><em>Puoi decidere di pagare tu, effettuando in autonomia i tuoi acquisti (senza doverci dare dettagli di spedizione ecc.) e farti rimborsare successivamente.</em></p>
		<p>
		<b>Vantaggi:</b><br />
			<ul>
				<li>puoi acquistare in maggiore autonomia con la certezza che sia tutto come volevi</li>
				<li>ideale se il carrello è molto complicato e difficilmente condivisibile, o simili situazioni in cui preferisci pagare da te</li>
			</ul>
		<b>Svantaggi:</b><br />
			<ul>
				<li>devi anticipare il pagamento</li>
				<li>devi ottenere una fattura intestata a te o comunque una ricevuta che dimostri l'acquisto da parte tua (per evitare che si rimborsino scontrini anonimi, pagati da altre persone estranee)</li>
				<li>devi fornirci i tuoi estremi bancari per il rimborso</li>
			</ul>
		</p>
	</div>
</div>

## Come Inviare la tua Richiesta

1. Se hai dubbi, soprattutto se temi di non riuscire a seguire qualcuna delle linee guida, contattaci!
    * **email**: puoi scriverci in privato una email a segreteria@linux.it
    * **Telegram**: puoi anche contattare la community che aiuta nella segreria pubblica in <https://t.me/ItalianLinuxSociety/45>
2. Compila il "modulo dei rimborsi spese" con i dettagli della tua richiesta di sostegno economico e allega i preventivi
    * **[Modulo Rimborsi Spese](https://ilsmanager.linux.it/ng/refund)**
3. Nella sezione commenti del formulario, scegli se sarà Italian Linux Society a pagare direttamente le spese (opzione 1 - consigliata), oppure se anticiperai i pagamenti e ti rimborsiamo (opzione 2). 

## Approvazione

Le richieste di sostegno economico sono approvate dal [consiglio direttivo di Italian Linux Society](/info/#consiglio-direttivo).

Le iniziative che soddisfano questi requisiti sono solitamente approvate dal consiglio direttivo in circa 1-2 settimane.

È anche possibile ottenere l'approvazione direttamente da una [sezione locale](/sezionilocali/) di riferimento (se ha sufficienti fondi).
In ogni caso, per motivi legati allo statuto e alle responsabilità del direttivo, il direttivo ha comunque potere di veto sulle richieste, anche quelle approvate dalle sezioni locali.

Se hai dubbi sui requisiti, contattaci per chiarimenti (contatti a fondo pagina).

Una volta che il sostegno economico all'iniziativa è approvato, puoi richiedere che sia Italian Linux Society a pagare direttamente le spese (opzione 1 - consigliata), oppure puoi anticipare i pagamenti e ti rimborsiamo (opzione 2):

## Altro

Per eventuali altre tipologie di richieste di pagamenti/donazioni/acquisto leggermente fuori dagli schemi proposti, è comunque possibile proporre la cosa al direttivo, in base alle necessità, disponibilità e consenso.

Segnaliamo anche altri programmi di sostegno esterni ad Italian Linux Society, da parte di partner convenzionati o simili, e che spesso possono sostenere (o co-sostenere) il tuo progetto:

* [Microgrant di Wikimedia Italia](https://wiki.wikimedia.it/wiki/Microgrant) - per questioni legate a Wikimedia e OpenStreetMap
* [Project Call di FSFE](https://fsfe.org/contact/projects-call/projects-call.it.html) - per software libero, da poter compilare (però in lingua inglese)

## Contattaci

Per domande puoi:

- contattare pubblicamente Italian Linux Society per esempio tramite Telegram FOSS
  sezione "Segreteria" - [https://t.me/ItalianLinuxSociety/45](https://t.me/ItalianLinuxSociety/45)
- oppure contattare in privato segreteria@linux.it per un primo parere
- altri [contatti](/contatti/)
