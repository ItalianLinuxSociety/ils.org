---
layout: page
title: Bozza Statuto ILS Terzo Settore
---

<div class="alert alert-info" role="alert">
<p><b>Grazie per il feedback che avete dato sul nuovo statuto per diventare Ente del Terzo Settore!</b></p>
<p><b>La bozza è ora archiviata. I vostri feedback saranno integrati presto, in una nuova bozza.
      Grazie ai vostri feedback è probabilmente meglio procedere su un nuovo modello di statuto, proposto da un Centro Servizi per il Volontariato (quello di Torino dove abbiamo la sede legale).
      Ci risentiamo al prossimo giro di feedback!</b></p>
<p>Vedi il <a href="https://gitlab.com/ItalianLinuxSociety/direttivo/-/issues/5">contesto storico e domande frequenti su questa bozza</a>.
</div>

-----------

