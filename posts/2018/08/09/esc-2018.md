<!--
.. title: ESC 2018
.. slug: esc-2018
.. date: 2018-08-09 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage:
-->

Nei pressi di Venezia, dal 28 agosto al 2 settembre, anche quest'anno si terrà l'<a href="https://www.endsummercamp.org/" rel="nofollow">End Summer Camp</a> (per gli amici: ESC).

Quest'anno alla manifestazione parteciperà anche Italian Linux Society, con un talk legato al tema della decentralizzazione di Internet (recentemente affrontato in modo diretto con <a href="{% link _posts/2018-05-14-servizi-linux-it.md %}">la pubblicazione di Servizi.Linux.it</a>) e, soprattutto, un follow up della <a href="{% link _posts/2018-01-30-lugconf-2018.md %}">LUGConf svolta a marzo a Torino</a>. Dedicato sia ad informare chi non c'era su quanto è stato discusso che a fornire qualche aggiornamento tecnico, nonché ad approfondire dal punto di vista amministrativo <a href="{% link _posts/2018-03-29-report-lugconf-2018.md %}">il progetto delle "sezioni locali"</a> di ILS destinato a facilitare la vita dei LUG nei mesi e negli anni a venire.

Sono invitati a partecipare i rappresentanti dei Linux Users Groups del nord-est, e ovviamente tutti coloro che sono interessati all'operatività della community italiana di promozione e divulgazione del software libero.