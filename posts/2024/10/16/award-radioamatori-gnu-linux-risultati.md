<!--
.. title: Il 1° award radio GNU/Linux al mondo
.. slug: primo-award-radioamatori-gnu-linux-al-mondo
.. date: 2024-10-16 07:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage: /images/posts/logo_diploma_linux_2024_square.jpg
-->

Molte persone ci stanno chiedendo come si è concluso il **diploma radioamatoriale dedicato a GNU/Linux** che si è tenuto dal 9 al 22 settembre 2024...

<!-- TEASER_END -->

Innanzitutto, a memoria di ogni persona che ha partecipato, possiamo comunicare con ragionevole certezza che, sì, questo è stato il **primo diploma GNU/Linux per radioamatori <u>al mondo</u>!**

(Siamo naturalmente felici di essere eventualmente smentiti... quindi [contatta Italian Linux Society](/contatti/) se hai testimonianze diverse!)

### Funzionamento, punteggi e premi

Il funzionamento era molto semplice ma sfidante. Per partecipare bisognava cercare di effettuare il maggior numero possibile di collegamenti radio.
Ognuno di questi collegamenti faceva guadagnare punti. Ogni punto poteva far guadagnare dei premi. Alcuni di questi premi erano anche delle targhette fisiche,
ideate appositamente!

Immaginate radioamatori da tutto il mondo che per due settimane parlano di software libero con la community italiana.

Sono stati assegnati punteggi diversi a seconda del mezzo trasmissivo e i diplomi hanno coperto tre categorie: **Bronzo**, **Argento** e **Oro**.

Le **Stazioni Jolly** hanno offerto punti extra per ogni "QSO" (collegamento) effettuato.

Le stazioni attivatrici hanno accettato chiamate generali sulle bande di loro competenza, fra cui:

- **CW:** "CQ LINUX" o "CQ GNU"
- **Fonia:** "CQ Primo Award GNU/Linux"
- **Digitali:** "CQ Primo Award GNU/Linux" o formule adatte al modo digitale in uso.

I risultati sono stati pubblicati in tempo reale sul sito dedicato. Qualsiasi partecipante ha potuto controllare il proprio punteggio e ha potuto poi scaricare il diploma in formato PDF.

## Trasmissioni anche dal MOCA

L'attività si è svolta in parallelo con il **[MOCA Camp](https://moca.camp/)**, importante evento hacker di portata internazionale. La stazione speciale del MOCA (chiamata II6MOCA) è stata attiva dal 13 al 15 settembre, offrendo ulteriori opportunità di guadagnare punti e canalizzando ancora più visibilità all'iniziativa.

## I numeri: 81 persone, 4mila collegamenti, 2mila stazioni

Sono state premiate **81** persone, fra cui persone dal **Marocco**, dal **Giappone** e persino dall'**Indonesia**!

È pazzesco che qualche persona così lontana abbia partecipato e scaricato il diploma della nostra community italofona!

Ci ha colpito nel cuore un commento dall'attivista Franco detto "XJA":

> il primo diploma GNU/Linux è finito e già mi manca! Pensa che molte chiudevano il QSO con "viva Linux": che grande soddisfazione!

Davvero un'emozione, soprattutto dopo aver concluso con **4.496 collegamenti**, **2.455** stazioni diverse e **150** attestati.

Congratulazioni alla community radioamatori per i [risultati del primo diploma GNU/Linux per radioamatori](https://www.ik7xja.it/xja/primo_diploma_linux_conclusione.shtml)!

## "Linux" all'ispettorato territoriale italiano

Non tutti sanno che, in Italia, gli ispettorati territoriali gestiscono anche le richieste dei nominativi temporanei speciali per manifestazioni radiantistiche e per contest internazionali.
Ebbene, l'ispettorato si è trovato, fra le varie richieste, quella per la creazione del nominativo con suffisso "<code>LNX</code>". Vi ricorda niente di particolare se lo rileggete?
Questo nominativo è stato approvato. Guardatelo nei vari database radioamatori: <code>[codice II7**LNX** nel database QRZ](https://www.qrz.com/db/II7LNX)</code>.
Senza parlare anche del nominativo richiesto per l'evento MOCA ([II6MOCA](https://www.qrz.com/db/II6MOCA)). Geniale!

## Le persone volontarie da ringraziare

Il diploma è stato organizzato da **Radio Penisola Salentina**, **RPS DX TEAM**, e il **LUG di Albaredo per San Marco**, in collaborazione con [Italian Linux Society](/) e il patrocinio del comune di Albaredo per San Marco, in provincia di Sondrio.

Il diploma si è rivolto a tutti gli appassionati di radio, siano essi radioamatori o semplici "SWL" (shortwave listening) e si è reso disponibile su due bande: **Versione OM** e **Versione SWL**.

Un ringraziamento operativo particolare all'attivista **Franco (IK7XJA)** che ha fornito un'energia straordinaria al progetto e particolari complimenti ad **Alessandro (IK2SGP)** da Albaredo, per la straordinaria partecipazione e promozione. Ricordiamo l'indirizzo per iscriversi al relativo user group:

<https://t.me/ApSM_Conoscere_Linux>

## Crediti multimediali

Scarica la [foto del diploma radioamatori GNU/Linux](/images/posts/logo_diploma_linux_2024_square.jpg): di Franco "IK7XJA" e contributori in licenza libera [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.it).

## Riascolta i podcast

Non perderti i podcast, fra cui:

[🔊 **Episodio 37**: Diploma GNU Linux 2024, Parte 2: le voci dei protagonisti](https://video.linux.it/w/4DpY5a9LbzyReDmeahmsHh) (con saluti istituzionali del presidente di ILS)

[🔊 **Episodio 38**: Linux, Moca e Radioamatori... Cosa c'entrano caffè, informatica, Linux e Radioamatori?](https://video.linux.it/w/vmyBBdA1EYgqeu9ZyyxL3t)

[🔊 **Episodio 39**: Conclusione del primo diploma GNU/Linux](https://video.linux.it/w/7xXUbdkkW9rCBrhFkYk8Hi)

Ascolta tutti gli altri podcast su PeerTube:

<https://video.linux.it/c/radio_penisola_salentina/videos>

## Prosegui le celebrazioni al Linux Day! Sabato 26 ottobre 2024 - nella tua città!

<https://www.linuxday.it/2024/>
