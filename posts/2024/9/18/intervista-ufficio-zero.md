<!--
.. title: Intervista a Ufficio Zero Linux OS e patrocinio
.. slug: intervista-ufficio-zero
.. date: 2024-09-18 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy:
.. previewimage: /images/posts/direttivo_ils_2024.jpg
-->

Intervista a Ufficio Zero Linux OS con il nostro patrocinio

<!-- TEASER_END -->

Cos'è Ufficio Zero Linux OS?
=====================

**Ufficio Zero** è una distribuzione GNU/Linux nata in Italia e ripartita dal 16 aprile 2020 durante la pandemia COVID-19 per avere un computer perfettamente funzionante dal punto di vista di liberi professionisti, partite IVA, enti pubblici e privati, e scuole (e parrocchie!) per l'uso dei proprio computer personali o postazioni di lavoro; soprattutto pensata appunto per un **ufficio**, con varie necessità pratiche, anche legate alla firma digitale in Italia, ecc.

Ufficio Zero si presenta come uno strumento ovviamente libero da restrizioni di copyright, gratuito, completo e di facile comprensione, **amichevole** per chi viene da strumenti cosiddetti "proprietari" come Microsoft Windows o da macOS e dalle loro interfacce grafiche, a cui molte persone sono abituate.

Alcune informazioni chiave di Ufficio Zero Linux:

- Posizione globale DistroWatch: **212** (https://distrowatch.com/table.php?distribution=ufficiozero)
- Download medi a settimana annunciati: **5000**-**6000** circa
- Autore: **Julian Del Vecchio**
- Architettura 32 bit supportata: **sì**
- Mirror in Italia: **sì**

Recupero Hardware e Assistenza in Italia
================================

La distribuzione è anche focalizzata al recupero hardware, ovvero, permettere di **dare nuova vita** a computer "leggermente anziani" e che magari montavano a bordo una versione di Microsoft Windows non più supportata dalla casa madre. In questo modo evitiamo di buttare hardware perfettamente funzionante, semplicemente installando nuovo software. Il progetto Ufficio Zero Linux (chiamato "UZL" per gli amici) supporta ancora distribuzioni a 32bit, per esempio (cosa che non fa più neanche Ubuntu - come ben sapete).

Abbiamo quindi intercettato Julian Del Vecchio, attivista della distribuzione.

Intervistando Julian Del Vecchio (autore della distribuzione)
==================================

**Domanda**: Caro Julian, cosa ne pensi delle scuole che usano Microsoft Windows **piratato** ancora oggi, invece che legalizzarsi e adottare strumenti liberi?

**Risposta (Julian)**: Cosa?!? Microsoft piratato nelle scuole, senza licenze?? Esistono istituti che usano Windows contraffato? Sinceramente non ne avevo idea ma avendo creato una distribuzione, inutile dire cosa penso dell'argomento: potrebbero installare UZL e potrebbero farci qualsiasi cosa, anche grazie a strumenti come WineHQ (script autoinstallante) che dà massimo supporto a Microsoft Windows.
Noi Spingiamo per il software libero, anche se sappiamo che ogni tanto c'è resistenza al cambiamento, soprattutto se la gente è abituata a software proprietari. Infatti il vostro patrocinio serve anche per avere man forte su qualsiasi istituzione anche scolastica e facendo capire loro la validità e la flessibilità del nostro sistema operativo: installi il sistema (puoi addirittura fare le join in ActiveDirectory) e con sè porta un ulteriore repository con i driver delle stampanti e scanner (per la maggior parte open!) in modo che il sistema riconosca ed installi anche le stampanti senza bisogno di far nulla nel 90% dei casi.

----

**Domanda**: C'è una realtà particolare che beneficia di Ufficio Zero e che vi ha portato grandi soddisfazioni?

**Risposta (Julian)**: La risposta potrà sorprendere ma NON conosciamo chi usa la nostra distribuzione: non trattiamo questi dati e vogliamo rendere UZL universale cioè vogliamo venga usato in molti settori. Questo è il nostro obiettivo e non siamo abituati a raccogliere dati magari violando la privacy altrui. Per esempio mi ha fatto piacere quando ci scrisse un tecnico informatico di una scuola di Cesena chiedendoci notizie per inserire UZL in domini Microsoft e gli abbiamo risposto che nei mirror è presente il tool "CID Closed in Directory" di uno sviluppatore brasiliano che faceva al caso suo. Tendenzialmente non lavoriamo con PC e server Microsoft, odiamo tutto della politica commerciale, di policy ed aggiornamenti Microsoft, ma bisogna lavorarci per aiutare le persone a migrare via.

----

**Domanda**: Ottimo! Sarete presenti in qualche evento prossimamente? :smirk:

**Risposta (Julian)**: Domanda trabocchetto! Ovviamente saremo presenti al prossimo [Linux Day di Brescia](https://www.ufficiozero.org/index.php?alias=linuxday-2024-brescia-programma) per un breve talk. Vi aspettiamo! :rainbow:

Patrocinio con Italian Linux Society
===========================

Ringraziamo Julian per il suo tempo e ricordiamo a chi legge che Ufficio Zero Linux OS (come molti altri meravigliosi progetti) è da oggi **patrocinato** dall'associazione di volontariato Italian Linux Society. Questo non solo per dare maggiore visibilità al progetto ma anche per fare rete, per **promuovere** l'uso di **software libero** e open source in Italia, in maniera ancora più capillare rispetto a come potremmo fare ognuno da solo a casa propria.

Inoltre, ricordiamo che Ufficio Zero Linux OS è accessibile anche attraverso il **mirror GARR**, garantendo un download molto rapido e molto sicuro per le utenze. GARR, che a sua volta è partner di Italian Linux Society.

Altre Informazioni
=============

Per una rapida occhiata al progetto consigliamo la visione di questo video, rilasciato da AlternativaLinux in **licenza libera**, Creative Commons, Attribuzione. P.S. Attenzione: YouTube è un servizio proprietario e richiede JavaScript proprietario, ma puoi scaricare legalmente quello specifico video per qualsiasi scopo :+1:

Link: [https://www.youtube.com/watch?v=SftfuirBhRI](https://www.youtube.com/watch?v=SftfuirBhRI)

Prossimi Passi
===========

- Prova [Ufficio Zero Linux](https://ufficiozero.org/)
- Unisciti a [Italian Linux Society](https://www.ils.org/info/) e racconta il tuo progetto!  :penguin:

## Crediti fotografici

Foto con licenza CC BY 2.0 di [pestoverde](https://openverse.org/image/collection?source=flickr&creator=pestoverde) da [Openverse](https://openverse.org/image/b0d2d66e-4452-4a8f-b57b-598aa3240718), modificata usando il software libero di fotoritocco [GIMP](https://it.wikipedia.org/wiki/GIMP).
