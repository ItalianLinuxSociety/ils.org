<!--
.. title: Bollettino 2024, 30 anni e oltre!
.. slug: bollettino-2024
.. date: 2025-01-11 12:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy:
.. previewimage: /images/posts/bollettino.jpg
-->

Siamo entrati nel 30° anno di presenza di Italian Linux Society, una comunità crescente che non si è mai fermata!
Questo è il bollettino del trascorso 2024, con info dai primi 9 mesi di nuovo direttivo, condividendo risultati e prossime sfide.
<!-- TEASER_END -->


<div class="summary" style="background: #eee; border-radius: 8px; border: 1px solid #ccc; padding: 2em;">
	Per chi ha fretta:

	<ul>
		<li>
			<a href="#sostegno-economico-alle-iniziative"><b>#Sostegno economico alle iniziative</b></a><br />
			ILS si è esposta nel dare ancora più <b>sostegno economico alle attività locali</b> a gruppi e persone singole, con <a href="/info/rimborsi/" target="_blank">linee guida sui rimborsi spese</a>, brevi e pratiche.
		</li>
		
		<li>
		  <a href="#nuove-funzionalita-su-ils-manager"><b>#Nuove funzionalità in ILS Manager</b></a><br />
			abbiamo investito <b>fondi sullo sviluppo</b> di una decina di funzionalità ad alta priorità per il nostro invidiato gestionale <b>ILS Manager</b>, migliorando l'organizzazione e la trasparenza ai soci e per poter centralizzare vari servizi futuri.
		</li>

		<li>
		  <a href="#nuove-sezioni-locali-nuovo-regolamento"><b>#Nuove sezioni locali, nuovo regolamento</b></a><br />
		  Con il nuovo regolamento delle sezioni locali e con la città di Fabriano, siamo arrivati a quota 18 sezioni locali. Hai già fatto il report semestrale? :D
	  </li>

		<li>
		  <a href="#nuovi-contenuti-per-linuxit"><b>#Nuovi contenuti per Linux.it</b></a><br />
			Abbiamo investito fondi per la <b>riscrittura dei contenuti del sito Linux.it</b> per renderlo un punto di atterraggio più utile, più organizzato e accentrare molti contenuti di alcuni sotto-siti difficili da mantenere. Vorresti aiutare su Linux.it? Dividiamoci il lavoro di scrittura!
		</li>

		<li>
		  <a href="#nuovo-statuto"><b>#Nuovo statuto</b></a><br />
			ILS diventerà <b>ente del terzo settore</b>, come <abbr title="Associazione di Promozione Sociale">"APS"</abbr>. La discussione sul vecchio statuto però si è protratta per così tanto tempo (dal 2022) che sembra proprio il caso di ripartire da un modello aggiornato, già revisionato, aggiornato e conforme, per non doversi preoccupare della sua validità e concludere in tempi certi, con maggiore consenso dai soci.
		</li>

		<li>
		  <a href="#eventi-in-arrivo"><b>#Eventi in arrivo</b></a><br />
		  FOSDEM, organizzazione MERGE-it, organizzazione Linux Day, ...
		</li>

		<li>
		  <a href="#come-aiutare"><b>#Come aiutare</b></a><br />
			Vuoi provare a dare una mano all'associazione? Scopri come!
			Per contribuire non è richiesto per forza una conoscenza di programmazione.
		</li>
	</ul>
</div>
---

## Sostegno economico alle iniziative

In vista del Linux Day 2024 ormai concluso, ma non solo, ma soprattutto ma anche in vista del Linux Day 2025 che si terrà il 25 ottobre 2025 (e sarà la grande 25esima edizione!) abbiamo migliorato le linee guida sui rimborsi spese di soci e gruppi locali. In particolare, oltre a <b>dettagli pratici su come richiedere risorse</b> ([https://www.ils.org/info/rimborsi/](https://www.ils.org/info/rimborsi/)), abbiamo definito un limite di rimborso massimo per i LUG e le sezioni locali pari a 1000 € / anno, mentre di 220 € / anno per certe spese dei singoli soc.
Serviva definire dei limiti chiari, per semplificare il lavoro di chi organizza attivitá e valutare in modo equo le richieste di sostegno che arrivavano e continuano ad arrivare.
Siamo molto felici di aver valutato… e approvato… tutte le richieste 2024! a volte non subito, a volte cercando un compromesso, a volte dando qualche consiglio per ottimizzare la spesa oppure per qualche dettaglio operativo... ma tutte sono state approvate, con <b>più di 7mila euro erogati</b> in progetti locali e Linux Day vari.
Questi rimborsi sono stati da poco pubblicati a tutti i soci, con i relativi report (alcuni ancora in corso di scrittura):

<https://ilsmanager.linux.it/ng/refund>

Approfittiamo per ricordare che i fondi di ILS arrivano dagli sponsor ma principalmente dalle quote dei soci. I fondi sono quindi gestiti dal consiglio direttivo (ruolo non pagato) e arrivano dalle persone associate e vengono allocati verso i progetti di chi si attiva per fare attività di promozione (e non solo) nel territorio, facendo volontariato, per cui ci auguriamo rimanga questo ottimo equilibrio e parsimonia nel richiederli, soprattutto perché finanziare le attività sul territorio è stato il capitolo di spesa più consistente con:

- realizzazione 52 Linux Day:<br /><https://www.linuxday.it/2024/>
- circa il doppio dei rimborsi spese gestiti ed approvati. Siamo a quota <b>40 rimborsi l'anno</b>, alcuni rientrano nei Linux Day, altri rimborsi a singole persone associate, sezioni locali ILS e LUG per spese varie. Nel 2023 erano appena la metà. Nel 2022 erano la metà della metà. Ora sono visibili ai soci per maggiore trasparenza e collaborazione:<br /><https://ilsmanager.linux.it/ng/refund>
- spedizione di adesivi e materiale promozionale a più di un centinaio di persone grazie al nuovo servizio postale gestito dalla suprema direzione spedizioni da parte del volontario Ferdinando Traversa da Bari:<br /><https://ilsmanager.linux.it/ng/gadget-request>
---

## Nuove sezioni locali, nuovo regolamento

La riforma del terzo settore è stata una mazzata per alcuni LUG che, dopo il periodo COVID-19, hanno avuto difficoltà nel riprendere attività in presenza, questo perché sono diminuiti i momenti di ritrovo fra le persone e ha aumentato gli obblighi derivanti dalle nuove normative per le associazioni.
Alcune associazioni hanno preferito chiudere, per risparmiare sul commercialista etc. per diventare sezione locale ILS, cogliendo questa àncora di salvezza nata già dal direttivo precedente, capitanato da Roberto Guido.

Per gestire questo cambiamento abbiamo fatto avanzare il regolamento delle sezioni locali, chiarendo le modalità di proposta, chiarendo i vantaggi, chiarendo cosa è richiesto fare come contributo nazionale, introducendo sperimentalmente i <b>report semestrali</b>, ovvero articoli che le sezioni pubblicheranno sui rispettivi siti web, per mantenere ancora più vivi questi siti web, dando ancora più visibilità alle attività svolte e aumentare l'impatto sulla comunicazione nazionale. Vedi l'ultima versione stabile del regolamento:

<https://www.ils.org/sezionilocali/>

Queste variazioni hanno accompagnato l'aumento a <b>18 sezioni locali</b>! Ancora un ringraziamento alle nuove arrivate, in ordine dalla più recente:

- [Sezione ILS Fabriano](/sezionilocali/fabriano/) - referente Luca Ferroni
- [Sezione ILS Genova](/sezionilocali/genova/) - referente Giovanni Longo
- [Sezione ILS Prato](/sezionilocali/prato/) - referente Alessandro Grassi
- [Sezione ILS Vicenza](/sezionilocali/vicenza/) - referente Bruno Zaupa
- [Sezione ILS Pavia](/sezionilocali/pavia/) - referente Julian Del Vecchio
- [Sezione ILS Imperia](/sezionilocali/imperia/) - referente Gianni Rosati
- [Sezione ILS Arona](/sezionilocali/arona/) - referente David Giovannini
- [Sezione ILS Roma](/sezionilocali/roma/) - referente Emanuele Savo
- e le altre più storiche! vedi [tutte le sezioni ILS](https://www.ils.org/sezionilocali/)

Questo è un grande successo per la community: è sempre più facile raccogliere donazioni da un'area geografica senza aprire dozzine di associazioni, per poi distribuire più facilmente queste risorse sulle relative attività delle persone volontarie.
Chi coordina le sezioni può anche ricevere la delega per molta burocrazia, indispensabile per fare attività sul territorio, come per le richieste di patrocini, richieste dagli user group ai vari enti pubblici.

## Nuova procedura di richiesta adesivi

Un altro aspetto che stava andando fuori controllo erano le spedizioni di adesivi e gadget: il modulo da compilare per richiederli inviava una semplice email a chi si doveva occupare della spedizione. Con l'aumentare di queste richieste si era creata una situazione dove non era semplice capire a che punto fossero le spedizioni già inviate o meno, e non c'era comunque alcun feedback verso chi aveva fatto la richiesta. Oggi sono tutte archiviate in ILS Manager, con notifiche email in tempo reale in caso di accettazione, presa in carico e spedizione, con stato di avanzamento e anche la possibilità di vedere il tracciamento postale.
Oltre al software, dobbiamo ringraziare dalla **sezione locale di ILS Bari**, il volontario Ferdinando Traversa, colui si è gentilmente messo a disposizione per spedire queste lettere, con i kit di promozione che vengono richiesti da Linux.it:
	
- <https://ilsmanager.linux.it/ng/gadget-request>
- <https://www.linux.it/stickers/>

## Nuove funzionalità su ILS Manager

Il nostro gestionale interno all'associazione, [ILS Manager](https://ilsmanager.linux.it/), progettato dal volontario Roberto Guido, era già un software notevole.
Quindi, il direttivo ha deciso di evolverlo, individuando 10 funzionalità ad alta priorità da implementare velocemente, possibilmente senza mandare in burnout il volontario Roberto.
Quindi, abbiamo cercato una persona professionista esterna all'associazione esperta in Laravel per diversi mesi, incaricando poi Valerio Galano, per sviluppare 10 funzionalità, fra cui:

- nuovi <b>promemoria automatici di versamento quote</b> via email ai soci, in base alla propria situazione individuale! per evitare di dover poi recuperare due anni di quote in un solo colpo, ecc.<br />
  <https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/65>
- nuova gestione <b>richieste gadget e spedizioni</b>:<br />per migliorare la diffusione di materiale promozionale, sia per chi li chiede, che per chi spedisce
  <https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/77>
- aggiunta delle <b>ricevute delle donazioni</b>, e link per poterne effettuare, dal proprio profilo utente
  <https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/92>
- aggiunta <b>autenticazione OAuth</b> su ILS Manager<br />per futuro accesso a istanza Nextcloud per soci e LUG. Questo, revisionando ed espandendo il lavoro già portato avanti dal gentilissimo volontario Marco Acorte.
  <https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/22>
- permettere ai referenti delle sezioni locali di revisionare i rimborsi spese sotto le proprie competenze<br />
  <https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/32>
- e molto altro! Vedi tutte le 10 funzionalità introdotte da Valerio Galano:<br />
  <https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/milestones/1>

Provate queste funzionalità! Fateci sapere. Miglioriamo la gestione interna dell'associazione.

## Nuovi contenuti per Linux.it

Dopo esserci occupati dell'organizzazione interna, ci siamo occupati della comunicazione esterna: [Linux.it](https://www.linux.it). Abbiamo incaricato una persona esperta in scrittura di contenuti web, identificando Carlotta, professionista che ha rivisto e riscritto tutti i testi del sito.
Il risultato che si otterrà sarà un sito che, per ora, manterrà la grafica dei menu e lo stile attuali, ma conterrà tantissimi contenuti che prima erano sparsi, alcuni orfani (non collegati in alcun menu), riorganizzando sottodomini come *scuola.linux.it* che saranno inglobabili come potenziali sotto-pagine di Linux.it - per evolverci come punto di riferimento della comunità di GNU/Linux e del software libero in Italia!

Ecco come aiutare sul nuovo menu di Linux.it e i nuovi contenuti:

<https://gitlab.com/ItalianLinuxSociety/linux.it/-/issues/19>

Stiamo preparando un branch apposito. Se hai del tempo libero per provare a lavorare su questo sito, fallo sapere nel gruppo Telegram:

<https://t.me/ItalianLinuxSociety/2> (PS: c'è anche la stanza Matrix sincronizzata)

## Nuovo statuto

Siccome la "nuova bozza" di nuovo statuto che ILS aveva ereditato dal 2022 e che abbiamo portato sotto la revisione di tutti i soci nel nostro mandato 2024 ha però evidenziato diverse criticità; viene molta voglia di ripartire da un modello standard, verificato, fornito dal Centro Servizi di Volontariato più vicino alla nostra sede legale (Torino), a cui apportare le (poche) modifiche di cui abbiamo bisogno.
Questo incidentalmente porterà probabilmente ad un risparmio sul budget previsto, rispetto al preventivo sullo statuto dal consulente precedente (che non ha potuto iniziare la sua consulenza 2024 nei tempi attesi per vari motivi).

Ringraziamo in particolare i soci Bruno Zaupa e Lorenzo Losa per l'aiuto in lista soci. Tutto lo storico sulla questione:

<https://gitlab.com/ItalianLinuxSociety/direttivo/-/issues/5>

In sostanza, rimaniamo tutti ancora un momento in attesa, e ringraziamo anticipatamente il centro servizi <a href="https://volontariatotorino.it/">VOL.TO</a> che ci inoltrerà presto un riscontro. A collaborazione iniziata con il CSV, lo statuto sarà probabilmente pronto fra meno di un mese, per revisione finale ai soci. Grazie per l'attesa!

## Eventi in arrivo

Potrebbe interressarvi:

- **[FOSDEM 2025](https://fosdem.org/2025/)** - 1 e 2 febbraio<br />
  La conferenza FOSDEM l'anno scorso ha portato 12.000+ persone a Bruxelles e anche quest'anno ospiterà molti partecipanti italofoni.<br />
  Ri-confermata la postazione del motore di videogiochi [Luanti](https://www.luanti.org/) (ex Minetest)! Complimenti al socio Zughy!<br />
  (altri stand italiani legati alla sicurezza informatica, ahinoi, non confermati)<br />
    - vi segnaliamo qualche evento pianificato, tipo:
        - <b>Tiro con l'ascia</b> liberatorio a tema "software proprietario"<br />
          organizzato da Italian Linux Society grazie al lanciatore Marcello Il Vichingo dal Free Circle.<br />
          P.S. Nessun software proprietario specifico sarà maltrattato durante l'evento.<br />
          Data e orario esatti da confermare. Per domande:<br />
          <https://t.me/ItalianLinuxSociety/9809>
- **Linux Winter Day** - 8 febbraio<br />
  Anche quest'anno, presso la scuola secondaria di I grado di Albino (Bergamo), si organizzerà il Linux Day invernale. C'è spazio per proporre contenuti.
  Per informazioni:<br />
  <https://t.me/LinuxWinterDayAlbino>
- **[MERGE-it](https://merge-it.net/)** (in definizione)<br />
  La scorsa conferenza MERGE-it ha permesso a varie community legate alle libertà digitali (software e contenuti) italofone di scambiarsi le ultime novità e discutere temi spinosi dal vivo.
  La [Fondazione Edulife di Verona](https://www.fondazioneedulife.org/) avrebbe il piacere di ri-ospitare un MERGE-it 2025,
  a patto di non lasciarli da soli e di aiutarli a prendere il meglio dall'esperienza passata.
  Ringraziamoli, diamo loro quindi una mano, intanto, scegliendo una potenziale data!<br />
  <https://t.me/+nKh1UGPGZJ03YmZk>
- **Linux Day 2025**<br />
  Annunciamo subito la data del prossimo Linux Day, facile da ricordare: 25 ottobre 2025. Sarà la 25esima epica edizione. Salvate la data! Iniziamo ad organizzarlo presto!

Attenzione: segui i siti web ufficiali, queste informazioni potrebbero cambiare.

## Come aiutare

Sono passati appena 9 mesi dal nostro nuovo ingresso nel consiglio direttivo e abbiamo ancora tanta voglia di avanzare ILS ma <b>è necessario l'aiuto</b> di ogni persona che <b>ama il software libero</b>.

Nella <b>Bacheca dei ticket</b> abbiamo riorganizzato per tipologia tutte le cose da fare se volete farvi un'idea della roadmap, e ne potete proporre di nuove:

<https://gitlab.com/groups/ItalianLinuxSociety/-/boards>

🐧🎄 Grazie del tuo aiuto, anche nel 2025! buon Software Libero!

## Crediti fotografici

L'immagine dell'articolo è rilasciata in licenza libera [CC0](https://creativecommons.org/public-domain/cc0/) dall'utente Fabio Lovato usando il software libero di fotoritocco [GIMP](https://it.wikipedia.org/wiki/GIMP) ed è una modifica dell'[immagine originale](https://stocksnap.io/photo/checklist-goals-TBJ9OPDGMK) dell'autore [Glenn Carstens-Peters](https://stocksnap.io/author/32960).

